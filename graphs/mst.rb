# frozen_string_literal: true

require 'rubygems'
require 'csv'
require 'set'
require 'pry'
require 'stringio'

require_relative 'shortest/p_queue'

class Node
  attr_accessor :name, :neighbors, :key, :parent

  class << self
    attr_accessor :node_hash, :node_list

    def add(u, v, w)
      @node_hash ||= {}
      @node_list ||= SortedSet.new
      @node_hash[u] ||= new(u)
      @node_hash[v] ||= new(v)
      @node_hash[u].neighbors[v] = w
      @node_hash[v].neighbors[u] = w
      @node_list << @node_hash[u]
      @node_list << @node_hash[v]
    end

    def extract_min
      node = @node_list.first
      @node_list = @node_list.drop(1)
      node
    end

    def empty?
      @node_list.empty?
    end

    def all
      @node_hash.values
    end

    def [](name)
      @node_list.find { |node| node.name == name }
    end
  end

  def initialize(name)
    @name = name
    @neighbors = {}
    @key = name == 'a' ? 0 : Float::INFINITY
    @parent = nil
  end

  def eql?(other)
    name == other.name
  end

  def <=>(other)
    key <=> other.key
  end
end

class MST
  def self.show_nodes(nodes, curr)
    $stderr.puts
    $stderr.puts
    $stderr.puts "curr: #{curr}"

    nodes.sort_by(&:key).each do |node|
      $stderr.puts "#{node.name} #{node.key > 10000 ? '-' : node.key} #{node.parent&.name || '-'}"
    end
  end

  def self.go
    edges = CSV.read('./mst.csv')

    node_names = edges.map {|edge| edge.take(2) }.flatten.uniq
    nodes = node_names.map { |name| Node.new(name) }

    adj = {}

    edges.each do |(name_a, name_b, weight)|
      node_a = nodes.detect { |node| node.name == name_a }
      node_b = nodes.detect { |node| node.name == name_b }
      adj[name_a] ||= {}
      adj[name_a][node_b] = weight.to_i
      adj[name_b] ||= {}
      adj[name_b][node_a] = weight.to_i
    end

    queue = PQueue.new(:key)
    all_nodes = []
    nodes.each { |node| queue.insert(node); all_nodes << node }
    is_in_q = Hash.new(true)

    until queue.empty? do
      node_u = queue.extract_first
      is_in_q[node_u.name] = false
                                                  # $stderr.puts "u: #{node_u.name} [key = #{node_u.key}]"
      adj[node_u.name].each do |(v, w)|
                                                  # $stderr.print "  neighbor: #{v} [weight = #{w}]"
        if is_in_q[v.name] && w < v.key
                                                  # $stderr.puts " ADDED"
          v.parent = node_u
          v.key = w
          queue.find_and_decrease(v)
        else
                                                  # $stderr.puts " SKIPPED"
        end
      end

      show_nodes(all_nodes, node_u.name)
    end

    m = ->(z) { z * 50 }
    positions = {
      'a' => [1, 3].map(&m),
      'b' => [3, 5].map(&m),
      'c' => [5, 5].map(&m),
      'd' => [7, 5].map(&m),
      'e' => [9, 3].map(&m),
      'f' => [7, 1].map(&m),
      'g' => [5, 1].map(&m),
      'h' => [3, 1].map(&m),
      'i' => [4, 3].map(&m)
    }

    output = StringIO.new

    output.puts 'graph G {'
    all_nodes.each do |v|
      # Place Node
      output.puts "  #{v.name} [label=\"#{v.name}\";pos=\"#{positions[v.name].join(',')}!\"]"

      next unless v.parent

      # Show MST edge
      output.puts "  #{v.name} -- #{v.parent.name} [label=#{v.key}];"
    end

    # original_edges.each do |(node_a, node_b, weight)|
    #   next if Node.node_hash[node_a].parent&.name == node_b || Node.node_hash[node_b].parent&.name == node_a

    #   output.puts "  #{node_a} -- #{node_b} [label=#{weight},fontcolor=cadetblue3;color=cadetblue3;style=dashed];"
    # end

    output.puts '}'
    output.string
  end
end
