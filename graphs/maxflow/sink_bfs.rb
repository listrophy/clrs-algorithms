# frozen_string_literal: true

require 'pry'
require 'rspec'
require 'csv'
require_relative './node'
require_relative './edge'

class SinkBFS
  attr_accessor :edges, :s, :t, :vertices, :colors, :d, :parents, :queue

  def initialize(edges, s, t)
    @edges = edges
    @s = s
    @t = t
    @vertices = edges.keys.flatten.uniq
    @colors = Hash[@vertices.map { |u| [u, :white] }]
    @d = Hash[@vertices.map { |u| [u, Float::INFINITY] }]
    @parents = Hash[@vertices.map { |u| [u, nil] }]
    @colors[s.name] = :gray
    @d[s.name] = 0
    @queue = [s]
  end

  def compute
    catch(:done) do
      while !queue.empty?
        u = queue.shift
        u.neighbors.each_pair do |v, edge|
          next if edge.capacity == edge.flow

          if @colors[v.name] == :white
            @colors[v.name] = :gray
            @d[v.name] = @d[u.name] + 1
            @parents[v.name] = u
            throw :done if v == t
            queue.push(v)
          end
        end
        @colors[u] = :black
      end
      return []
    end

    result = [t]
    begin
      result.unshift(@parents[result.first.name])
    end until result.first == s
    result
  end
end

RSpec.describe SinkBFS do
  let(:input) { CSV.read('./graph.csv') }
  let(:nodes) do
    n = {}
    input.each do |(u,v,_)|
      n[u] ||= Node.new(u)
      n[v] ||= Node.new(v)
    end
    n
  end

  let(:edges) do
    e = {}
    input.each do |(u,v,c)|
      e[[u,v]] = Edge.new(nodes[u], nodes[v], c)
    end
    e
  end
  let(:s) { nodes['s'] }
  let(:t) { nodes['t'] }

  before do
    nodes
    edges
  end

  subject(:bfs) { described_class.new(edges, s, t) }

  it 'works' do
    expect(bfs.compute.map(&:name)).to eq(%w[s a b t])
  end

  context 'with empty' do
    let(:input) { [%w[s b 4], %w[c t 2]] }

    it 'works' do
      expect(bfs.compute).to eq([])
    end
  end

  context 'with full' do
    let(:input) { [%w[s b 4], %[b c 0], %w[c t 2]] }

    it 'works' do
      expect(bfs.compute).to eq([])
    end
  end
end
