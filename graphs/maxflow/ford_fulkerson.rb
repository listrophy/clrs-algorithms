# frozen_string_literal: true

require 'csv'
require_relative './sink_bfs'
require_relative './node'
require_relative './edge'

input = CSV.read('./graph.csv')

nodes = {}
input.each do |(u,v,_)|
  nodes[u] ||= Node.new(u)
  nodes[v] ||= Node.new(v)
end
s = nodes['s']
t = nodes['t']

edges = {}
input.each do |(u,v,c)|
  existing = edges[[u,v]]
  if existing.nil? || existing.capacity == 0
    edges[[u, v]] = Edge.new(nodes[u], nodes[v], c)
  end

  existing = edges[[v,u]]
  if existing.nil?
    edges[[v, u]] = Edge.new(nodes[v], nodes[u], 0)
    edges[[v, u]].residual = true
  end
end

def find_path(s, t, edges)
  SinkBFS.new(edges, s, t).compute
end

def segments(path)
  path.each_cons(2)
end

loop do
  # find any path from s to t
  p = find_path(s, t,
    edges.reject {|_names, e| e.capacity == 0 })

  break if p.empty?

  # find the segement with least remaining capacity
  segs = segments(p)
  residual_capacity_p = segs.map do |uv|
    edges[uv.map(&:name)].residual_capacity
  end.min

  segs.each do |(u, v)|
    # add that capacity to each edge
    edges[[u.name, v.name]].flow += residual_capacity_p
    # if we need to reverse the flow in the future:
    edges[[v.name, u.name]].flow = -edges[[u.name, v.name]].flow
  end
end

m = ->(x) { x * 60 }
positions = {
  's' => [1, 3].map(&m),
  'a' => [3, 5].map(&m),
  'b' => [5, 5].map(&m),
  'c' => [3, 1].map(&m),
  'd' => [5, 1].map(&m),
  't' => [7, 3].map(&m)
}

puts "digraph G {"

positions.each do |v, pos|
  puts "  #{v} [pos=\"#{pos.join(',')}!\"];"
end

edges.each_pair do |(u, v), edge|
  next if edge.residual
  puts "  #{u} -> #{v} [label=#{edge.flow}];"
end
puts "}"
