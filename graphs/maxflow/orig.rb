require 'csv'

m = ->(x) { x * 60 }
positions = {
  's' => [1, 3].map(&m),
  'a' => [3, 5].map(&m),
  'b' => [5, 5].map(&m),
  'c' => [3, 1].map(&m),
  'd' => [5, 1].map(&m),
  't' => [7, 3].map(&m)
}

puts 'digraph G {'
positions.each do |v, pos|
  puts "  #{v} [pos=\"#{pos.join(',')}!\"];"
end

CSV.read('./graph.csv').each do |(a, b, w)|
  puts "  #{a} -> #{b} [label=#{w}];"
end

puts "}"
