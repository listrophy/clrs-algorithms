# frozen_string_literal: true

class Edge
  attr_accessor :u, :v, :capacity, :flow, :residual

  def initialize(u, v, c)
    @u = u
    @v = v
    @capacity = c.to_i
    @flow = 0
    u.neighbors[v] = self
    @residual = false
  end

  def residual_capacity
    capacity - flow
  end
end

