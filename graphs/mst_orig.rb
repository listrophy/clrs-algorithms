# frozen_string_literal: true

require 'csv'
require 'stringio'

class MSTOriginal
  def self.go
    m = ->(z) { z * 50 }
    positions = {
      'a' => [1, 3].map(&m),
      'b' => [3, 1].map(&m),
      'c' => [5, 1].map(&m),
      'd' => [7, 1].map(&m),
      'e' => [9, 3].map(&m),
      'f' => [7, 5].map(&m),
      'g' => [5, 5].map(&m),
      'h' => [3, 5].map(&m),
      'i' => [4, 3].map(&m)
    }

    output = StringIO.new

    output.puts 'graph G {'

    positions.each_pair do |n, p|
      output.puts "  #{n} [label=\"#{n}\";pos=\"#{p.join(',')}!\"];"
    end

    CSV.read('mst.csv').each do |(node_a, node_b, weight)|
      output.puts "  #{node_a} -- #{node_b} [label=#{weight}];"
    end

    output.puts '}'
    output.string
  end
end
