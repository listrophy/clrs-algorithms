# frozen_string_literal: true

require 'rspec'
require 'pry'

class PQueue
  attr_accessor :items

  def initialize(comparator)
    @items = []
    @comparator = comparator
    @infinite_object = Object.new
    @infinite_object.define_singleton_method(comparator) do
      Float::INFINITY
    end
  end

  def empty?
    items.empty?
  end

  def first
    items.first
  end

  def extract_first
    raise "underflow" if items.empty?

    item = first
    last = items.pop
    items[0] = last unless items.empty?
    min_heapify(0)
    item
  end

  def key(object)
    object.public_send(@comparator)
  end

  def less_than(a, b)
    key(a) < key(b)
  end

  def find_and_decrease(object)
    index = @items.index {|item| item.name == object.name }
    decrease_key(index, object) if index
  end

  def decrease_key(i, object)
    raise 'new object is larger than current object' if less_than(items[i], object)

    items[i] = object
    while i > 0 && less_than(items[i], items[parent(i)])
      swap(i, parent(i))
      i = parent(i)
    end
  end

  def insert(object)
    items.push(@infinite_object)
    decrease_key(items.length - 1, object)
  end

  def min_heapify(i)
    l = left i
    r = right i
    smallest =
      if l < items.length && less_than(items[l], items[i])
        l
      else
        i
      end
    if r < items.length && less_than(items[r], items[smallest])
      smallest = r
    end

    if smallest != i
      swap(i, smallest)
      min_heapify(smallest)
    end
  end

  def left(i)
    2 * i + 1
  end

  def right(i)
    2 * i + 2
  end

  def parent(i)
    (i + 1) / 2 - 1
  end

  def swap(i, j)
    temp = items[i]
    items[i] = items[j]
    items[j] = temp
  end
end

class TestNode
  attr_accessor :key
  def initialize(key)
    @key = key
  end
end

RSpec.describe PQueue do
  subject(:queue) { described_class.new(:key) }

  let(:elems) { [1, 10, 3, 4, 2, 6, 8].map {|e| TestNode.new(e) } }

  it 'works' do
    elems.each {|x| queue.insert x }
    result = []
    until queue.first.nil?
      result << queue.extract_first
    end

    expect(result).to eq(elems.sort_by(&:key))
  end
end
