# frozen_string_literal: true

require 'csv'
require_relative 'p_queue'

class Node
  attr_accessor :name, :d, :parent

  def initialize(name)
    @name = name
    @d = name == 'a' ? 0 : Float::INFINITY
    @parent = nil
  end

  def relax(other, w)
    if other.d > (d + w)
      other.d = d + w
      other.parent = self
    end
  end
end

# input

edges = CSV.read('../mst.csv')

# initialize

node_names = edges.map {|edge| edge.take(2) }.flatten.uniq
nodes = node_names.map { |name| Node.new(name) }

adj = {}
edges.each do |(name_a, name_b, weight)|
  node_a = nodes.detect { |node| node.name == name_a }
  node_b = nodes.detect { |node| node.name == name_b }
  adj[name_a] ||= {}
  adj[name_a][node_b] = weight.to_i
  adj[name_b] ||= {}
  adj[name_b][node_a] = weight.to_i
end

# dijkstra

queue = PQueue.new(:d)
nodes.each { |node| queue.insert(node) }

until queue.empty?
  u = queue.extract_first
  adj[u.name].each_pair do |v, w|
    u.relax(v, w)
    queue.find_and_decrease(v)
  end

  puts
  puts
  puts "curr: #{u.name}"
  nodes.sort_by(&:name).each do |node|
    puts "#{node.parent&.name || ' '} <- #{node.name} [#{node.d}]"
  end
end

puts
puts
puts 'answer'
nodes.sort_by(&:name).each do |node|
  puts "#{node.parent&.name || ' '} <- #{node.name} [#{node.d}]"
end
