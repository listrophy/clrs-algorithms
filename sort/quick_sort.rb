# frozen_string_literal: true

require 'fiber'

class QuickSort
  include Enumerable

  attr_reader :vals, :params

  WIDTH = 14
  GUTTER = 3
  MULT = 4
  VMULT = MULT * 3

  def initialize(vals)
    @vals = vals
    @params = {}
    @locked = []
    @fiber = Fiber.new(&method(:sort))
  end

  def sort
    qsort(0, vals.length - 1)
    {}
  end

  def qsort(p, r)
    unless p < r
      @locked << p
      return
    end

    q = partition(p, r)
    qsort(p, q - 1)
    qsort(q + 1, r)
  end

  def partition(p, r)
    x = vals[r]
    i = p - 1
    Fiber.yield(p: p, r: r, i: i)
    (p...r).each do |j|
      if vals[j] <= x
        Fiber.yield(p: p, r: r, i: i, j: j, exchange: [i + 1, j])
        i += 1
        exchange(i, j)
        Fiber.yield(p: p, r: r, i: i - 1, j: j)
        Fiber.yield(p: p, r: r, i: i, j: j)
      else
        Fiber.yield(p: p, r: r, i: i, j: j)
      end
    end
    Fiber.yield(p: p, r: r, i: i, exchange: [i + 1, r])
    exchange(i + 1, r)
    @locked << (i + 1)
    i + 1
  end

  def exchange(i, j)
    temp = vals[i]
    @vals[i] = vals[j]
    @vals[j] = temp
  end

  def step
    @params = @fiber.resume if @fiber.alive?
  end

  def each(&block)
    vals.each(&block)
  end

  def draw(zelf)
    zelf.stack do
      vals.each_with_index do |val, idx|
        r = zelf.rect(top: VMULT * (5 + (10 - val)),
                      left: MULT * ((WIDTH + GUTTER) * idx + GUTTER),
                      width: MULT * WIDTH,
                      height: VMULT * val)
        r.style(fill: zelf.dimgray) if params[:p] && params[:r] && ((params[:p])..(params[:r])).include?(idx)
        r.style(fill: zelf.cadetblue) if idx == params[:r]
        r.style(fill: zelf.orangered) if idx == params[:j]
        r.style(fill: zelf.ivory) if @locked.include?(idx)

        zelf.stack(width: MULT * WIDTH, left: MULT * (WIDTH * idx + GUTTER * (idx + 1)), top: VMULT * 15) do
          zelf.inscription(val.to_s)
            .style(align: 'center', font: "#{MULT * 8}px")
        end
      end

      (params[:exchange] || []).each do |idx|
        zelf.star(MULT * (GUTTER + idx * (GUTTER + WIDTH) + WIDTH / 2),
                  VMULT * 20,
                  5, MULT * WIDTH / 2, MULT * WIDTH / 4).style(fill: zelf.ivory)
      end

      # the "i" partition line
      if params.key?(:i)
        i_width = GUTTER / 3
        i_x = MULT * ((@params[:i] + 1) * (WIDTH + GUTTER) + GUTTER / 2)
        zelf.line(i_x, 3 * VMULT, i_x, 16 * VMULT).style(stroke: zelf.darkmagenta, strokewidth: MULT * i_width)
      end

      # consideration bar
      zelf.rect(top: VMULT * 25,
                left: MULT * (GUTTER + params[:p] * (GUTTER + WIDTH)),
                right: MULT * ((params[:r] + 1) * (GUTTER + WIDTH)),
                height: VMULT * 2) unless params.empty?
    end
  end
end

class QuickSortMaster
  ARRAY = [8, 9, 2, 6, 10, 3, 4, 11, 1, 5, 7]

  def self.generate(zelf)
    zelf.window(title: "Yay!", width: QuickSort::MULT * (12 * QuickSort::GUTTER + 11 * QuickSort::WIDTH)) do
      move(0, 0)
      app.name = 'quicksort!'

      @data = QuickSort.new(ARRAY.dup)

      stack do
        button "step", font: "Menlo Bold 16", width: 100 do
          @data.step
          @flow.clear do
            @data.draw(self)
          end
        end

        @flow = flow do
          @data.draw(self)
        end
      end
    end
  end
end
