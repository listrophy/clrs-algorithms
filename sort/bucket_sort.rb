# frozen_string_literal: true

require 'fiber'

class BucketSort
  include Enumerable

  attr_reader :vals

  WIDTH = 14
  GUTTER = 3
  MULT = 4
  VMULT = MULT * 3

  def initialize(vals)
    @vals = vals
    @buckets = Array.new(vals.length, nil)
    @fiber = Fiber.new(&method(:sort))
  end

  def sort
    @vals.each_with_index do |val, idx|
      @buckets[val] = val
      Fiber.yield
    end
  end

  def step
    @params = @fiber.resume if @fiber.alive?
  end

  def each(&block)
    vals.each(&block)
  end

  def draw(zelf)
    zelf.flow do
      vals.each_with_index do |val, idx|
        r = zelf.rect(top: MULT * 5,
                      left: MULT * (GUTTER * (idx + 1) + WIDTH * idx),
                      width: MULT * WIDTH,
                      height: MULT * WIDTH)
        r.style(fill: zelf.black)
        t = zelf.para(val.to_s)
        t.style(align: 'center',
                top: MULT * 5 + MULT * WIDTH * 3 / 4,
                left: MULT * (GUTTER * (idx + 1) + WIDTH * idx),
                height: MULT * WIDTH / 2,
                font: "#{MULT * 8}px",
                width: MULT * WIDTH,
                stroke: zelf.ivory)
      end

      @buckets.each_with_index do |bucket, idx|
        down = 45
        r = zelf.rect(top: MULT * down,
                      left: MULT * (GUTTER * (idx + 1) + WIDTH * idx),
                      width: MULT * WIDTH,
                      height: MULT * WIDTH)
        r.style(fill: zelf.ivory)
        t = zelf.para(idx.to_s)
        t.style(align: 'center',
                top: MULT * (down + 11) + MULT * WIDTH * 3 / 4,
                left: MULT * (GUTTER * (idx + 1) + WIDTH * idx),
                height: MULT * WIDTH / 2,
                font: "#{MULT * 5}px",
                width: MULT * WIDTH,
                stroke: zelf.black)

        if bucket
          from = vals.index(bucket)
          e = zelf.para(bucket.to_s)
          e.style(align: 'center',
                  top: MULT * down + MULT * WIDTH * 3 / 4,
                  left: MULT * (GUTTER * (idx + 1) + WIDTH * idx),
                  height: MULT * WIDTH / 2,
                  font: "#{MULT * 8}px",
                  width: MULT * WIDTH,
                  stroke: zelf.black)
          zelf.line(MULT * (GUTTER * (from + 1) + WIDTH * (from + 0.5)),
                    MULT * 22,
                    MULT * (GUTTER * (idx + 1) + WIDTH * idx + 0.5 * WIDTH),
                    MULT * (down - 1)
                   ).style(strokewidth: 4)

        end
      end
    end
  end
end

class BucketSortMaster
  ARRAY = [7, 5, 1, 8, 9, 2, 3, 10, 0, 4, 6]

  def self.generate(zelf)
    zelf.window(title: "Yay!", width: BucketSort::MULT * (12 * BucketSort::GUTTER + 11 * BucketSort::WIDTH)) do
      move(0, 0)
      app.name = 'bucketsort!'

      @data = BucketSort.new(ARRAY.dup)

      stack do
        button "step", font: "Menlo Bold 16", width: 100 do
          @data.step
          @flow.clear do
            @data.draw(self)
          end
        end

        @flow = flow do
          @data.draw(self)
        end
      end
    end
  end
end
