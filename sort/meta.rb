Shoes.app title: "Base Window" do
  @insertion_window = nil
  button "Insertion Sort" do
    @insertion_window.close if @insertion_window
    Object.send(:remove_const, :InsertionSortMaster) if defined?(InsertionSortMaster)
    Object.send(:remove_const, :InsertionSort) if defined?(InsertionSort)
    load(File.expand_path('./insertion_sort.rb', __dir__))
    @insertion_window = InsertionSortMaster.generate(self)
  end

  button "Quick Sort" do
    @quick_window.close if @quick_window
    Object.send(:remove_const, :QuickSortMaster) if defined?(QuickSortMaster)
    Object.send(:remove_const, :QuickSort) if defined?(QuickSort)
    load(File.expand_path('./quick_sort.rb', __dir__))
    @quick_window = QuickSortMaster.generate(self)
  end

  button "Bucket Sort" do
    @bucket_window.close if @bucket_window
    Object.send(:remove_const, :BucketSortMaster) if defined?(BucketSortMaster)
    Object.send(:remove_const, :BucketSort) if defined?(BucketSort)
    load(File.expand_path('./bucket_sort.rb', __dir__))
    @bucket_window = BucketSortMaster.generate(self)
  end
end
