# frozen_string_literal: true

require 'fiber'

class InsertionSort
  include Enumerable

  attr_reader :vals, :subject, :i, :j

  WIDTH = 14
  GUTTER = 3
  MULT = 4
  VMULT = MULT * 3

  def initialize(vals)
    @vals = vals
    @i, @j = nil
    @fiber = Fiber.new(&method(:sort))
    @subject = nil
  end

  def sort
    (1...(@vals.length)).each do |index|
      @j = index
      @subject = vals[j]

      @i = j - 1
      while i >= 0 && (Fiber.yield; true) && vals[i] > subject
        @vals[i + 1] = vals[i]
        @i = i - 1
      end

      @vals[i + 1] = subject
    end
    @subject, @i, @j = nil
    Fiber.yield
  end

  def step
    @fiber.resume if @fiber.alive?
  end

  def each(&block)
    vals.each(&block)
  end

  def draw(zelf)
    zelf.stack do
      vals.each_with_index do |val, idx|
        is_temp = i && j && i != (j - 1) && idx == (i + 1)

        r = zelf.rect(top: VMULT * (5 + (10 - val)),
                      left: MULT * ((WIDTH + GUTTER) * idx + GUTTER),
                      width: MULT * WIDTH,
                      height: VMULT * val)
        if is_temp
          r.style(fill: zelf.ivory)
          zelf.rect(top: VMULT * (5 + (10 - subject)),
                    left: MULT * ((WIDTH + GUTTER) * idx + GUTTER),
                    width: MULT * WIDTH,
                    height: VMULT * subject,
                    fill: zelf.cadetblue)
        end
        r.style(fill: zelf.darkmagenta) if idx == i
        r.style(fill: zelf.orangered) if idx == j

        zelf.stack(width: MULT * WIDTH, left: MULT * (WIDTH * idx + GUTTER * (idx + 1)), top: VMULT * 15) do
          zelf.inscription(is_temp ? '?' : val.to_s)
            .style(align: 'center', font: "#{MULT * 8}px")
        end
      end
      zelf.para "subject: #{subject.nil? ? '<nil>' : subject}", font: "#{MULT * 8}px", align: 'center'
    end
  end
end

class InsertionSortMaster
  ARRAY = [6, 8, 2, 11, 7, 10, 3, 4, 5, 9, 1]

  def self.generate(zelf)
    zelf.window(title: "Yay!", width: InsertionSort::MULT * (12 * InsertionSort::GUTTER + 11 * InsertionSort::WIDTH)) do
      move(0, 0)
      app.name = 'sort!'

      @data = InsertionSort.new(ARRAY.dup)

      stack do
        button "step", font: "Menlo Bold 16", width: 100 do
          @data.step
          @flow.clear do
            @data.draw(self)
          end
        end

        @flow = flow do
          @data.draw(self)
        end
      end
    end
  end
end
