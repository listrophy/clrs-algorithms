# frozen_string_literal: true

require_relative './lcs'

RSpec.describe Lcs do
  {
    %w[abc acd] => 2,
    %w[abc acb] => 2,
    %w[abcdef bdf] => 3,
    %w[abcabcdef caddg] => 3,
    %w[XMJYAUZ MZJAWXU] => 4,
    %w[AAACCGTGAGTTATTCGTTCTAGAA CACCCCTAAGGTACCTTTGGTTC] => 14
  }.each do |input, output|
    specify "#{input.inspect} yields #{output}" do
      expect(described_class.compute(*input)).to eq output
    end
  end
end
