# frozen_string_literal: true

# Lcs: Longest Common Subsequence
# rubocop:disable Naming/UncommunicativeMethodParamName
class Lcs
  def self.compute(string_a, string_b)
    new(string_a, string_b).compute
  end

  def initialize(string_a, string_b)
    @a = string_a
    @b = string_b
  end

  def compute
    setup
    compute_matrix
    output_matrix
    @matrix.last.last
  end

  def setup
    @matrix = Array.new(@a.length + 1) { Array.new(@b.length + 1, 0) }
  end

  def compute_matrix
    (1..(@a.length)).each do |i|
      (1..(@b.length)).each do |j|
        compute_cell(i, j)
      end
    end
  end

  def compute_cell(i, j)
    @matrix[i][j] =
      if @a[i - 1] == @b[j - 1]
        matches(i, j)
      else
        does_not_match(i, j)
      end
  end

  def matches(i, j)
    @matrix[i - 1][j - 1] + 1
  end

  def does_not_match(i, j)
    [@matrix[i - 1][j], @matrix[i][j - 1]].max
  end

  def output_matrix
    width = @matrix.last.last.to_s.length
    puts
    # puts @matrix.inspect
    puts(@matrix.map do |row|
      row.map { |val| format("%#{width}d", val) }.join(' ')
    end.join("\n"))
    puts
  end
end
# rubocop:enable Naming/UncommunicativeMethodParamName
